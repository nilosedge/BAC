package net.nilosplace.BAC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.xeiam.xchange.dto.marketdata.Ticker;

public class DataSaver {
	
	private Connection sharedConnection;


	public DataSaver() {
		try {
			Class.forName("org.sqlite.JDBC");
			sharedConnection = DriverManager.getConnection("jdbc:sqlite:data.sqlite");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void saveData(Ticker mtgoxTicker, Ticker bitstampTicker) {
		try {
			sharedConnection.createStatement().execute("insert into datatable (mtgox, bitstamp, current_time) values(\"" + mtgoxTicker.getLast().getAmount() + "\", \"" + bitstampTicker.getLast().getAmount() + "\", \"" + mtgoxTicker.getTimestamp().getTime() + "\")");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

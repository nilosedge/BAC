package net.nilosplace.BAC.demos;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.dto.marketdata.OrderBookUpdate;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.dto.marketdata.Trade;
import com.xeiam.xchange.mtgox.v2.MtGoxExchange;
import com.xeiam.xchange.mtgox.v2.service.streaming.MtGoxStreamingConfiguration;
import com.xeiam.xchange.service.streaming.ExchangeEvent;
import com.xeiam.xchange.service.streaming.ExchangeStreamingConfiguration;
import com.xeiam.xchange.service.streaming.StreamingExchangeService;

public class MtGoxWebSocketMarketDataDemo {
	public static void main(String[] args) throws ExecutionException, InterruptedException {
		 
		MtGoxWebSocketMarketDataDemo streamingTickerDemo = new MtGoxWebSocketMarketDataDemo();
		streamingTickerDemo.start();
	}
	 
	public void start() throws ExecutionException, InterruptedException {
		Exchange mtGoxExchange = ExchangeFactory.INSTANCE.createExchange(MtGoxExchange.class.getName());
	 
		ExchangeStreamingConfiguration mtGoxStreamingConfiguration = new MtGoxStreamingConfiguration(10, 10000, 60000, true, "trade.BTCUSD");
			
		StreamingExchangeService streamingMarketDataService = mtGoxExchange.getStreamingExchangeService(mtGoxStreamingConfiguration);
			
		streamingMarketDataService.connect();
 
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		Future<?> mtGoxMarketDataFuture = executorService.submit(new MarketDataRunnable(streamingMarketDataService));
	 
		mtGoxMarketDataFuture.get();
	 
		executorService.shutdown();
	 
		System.out.println(Thread.currentThread().getName() + ": Disconnecting...");
		streamingMarketDataService.disconnect();
	}
	 
	class MarketDataRunnable implements Runnable {
	 
		private final StreamingExchangeService streamingExchangeService;
	 
		public MarketDataRunnable(StreamingExchangeService streamingExchangeService) {
			this.streamingExchangeService = streamingExchangeService;
		}

		public void run() {
			try {
				while (true) {
					ExchangeEvent exchangeEvent = streamingExchangeService.getNextEvent();
					switch (exchangeEvent.getEventType()) {
						case CONNECT:
							System.out.println("Connected!");
							break;
						case TICKER:
							Ticker ticker = (Ticker) exchangeEvent.getPayload();
							System.out.println(ticker.toString());
							break;
						case TRADE:
							Trade trade = (Trade) exchangeEvent.getPayload();
							System.out.println(trade.toString());
							break;
						case DEPTH:
							OrderBookUpdate orderBookUpdate = (OrderBookUpdate) exchangeEvent.getPayload();
							System.out.println(orderBookUpdate.toString());
							break;
						default:
							System.out.println(exchangeEvent.getEventType());
							break;
					}
				}
			} catch (InterruptedException e) {
				System.out.println("ERROR in Runnable!!!");
			}
		}
	}
}

package net.nilosplace.BAC.demos;

import java.io.IOException;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeException;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.NotAvailableFromExchangeException;
import com.xeiam.xchange.NotYetImplementedForExchangeException;
import com.xeiam.xchange.currency.Currencies;
import com.xeiam.xchange.dto.marketdata.OrderBook;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.dto.marketdata.Trades;
import com.xeiam.xchange.mtgox.v2.MtGoxExchange;
import com.xeiam.xchange.service.polling.PollingMarketDataService;

public class MtGoxMarketdataDemo {

	public static void main(String[] args) throws ExchangeException, NotAvailableFromExchangeException, NotYetImplementedForExchangeException, IOException {
	    // Demonstrate the public market data service
	    // Use the factory to get the version 2 MtGox exchange API using default settings
	    Exchange mtGoxExchange = ExchangeFactory.INSTANCE.createExchange(MtGoxExchange.class.getName());
	 
	    // Interested in the public market data feed (no authentication)
	    PollingMarketDataService marketDataService = mtGoxExchange.getPollingMarketDataService();
	 
	    // Get the latest ticker data showing BTC to USD
	    Ticker ticker = marketDataService.getTicker(Currencies.BTC, Currencies.USD);
	    String btcusd = ticker.getLast().toString();
	    System.out.println("Current exchange rate for BTC / USD: " + btcusd);
	 
	    // Get the current orderbook
	    OrderBook orderBook = marketDataService.getPartialOrderBook(Currencies.BTC, Currencies.USD);
	    System.out.println("Current Order Book size for BTC / USD: " + orderBook.getAsks().size() + orderBook.getBids().size());
	 
	    // Get the current full orderbook
	    OrderBook fullOrderBook = marketDataService.getFullOrderBook(Currencies.BTC, Currencies.USD);
	    System.out.println("Current Full Order Book size for BTC / USD: " + fullOrderBook.getAsks().size() + fullOrderBook.getBids().size());
	 
	    // Get trades
	    Trades trades = marketDataService.getTrades(Currencies.BTC, Currencies.PLN);
	    System.out.println("Current trades size for BTC / PLN: " + trades.getTrades().size());


	}

}

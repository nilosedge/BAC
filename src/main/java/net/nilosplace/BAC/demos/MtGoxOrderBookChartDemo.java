package net.nilosplace.BAC.demos;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeException;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.NotAvailableFromExchangeException;
import com.xeiam.xchange.NotYetImplementedForExchangeException;
import com.xeiam.xchange.currency.Currencies;
import com.xeiam.xchange.dto.marketdata.OrderBook;
import com.xeiam.xchange.dto.trade.LimitOrder;
import com.xeiam.xchange.mtgox.v2.MtGoxExchange;
import com.xeiam.xchange.service.polling.PollingMarketDataService;
import com.xeiam.xchart.Chart;
import com.xeiam.xchart.Series;
import com.xeiam.xchart.SeriesMarker;
import com.xeiam.xchart.SwingWrapper;



public class MtGoxOrderBookChartDemo {
	public static void main(String[] args) throws ExchangeException, NotAvailableFromExchangeException, NotYetImplementedForExchangeException, IOException {
		// Use the factory to get the version 1 MtGox exchange API using default settings
		Exchange mtGox = ExchangeFactory.INSTANCE.createExchange(MtGoxExchange.class.getName());
	 
		// Interested in the public market data feed (no authentication)
		PollingMarketDataService marketDataService = mtGox.getPollingMarketDataService();
	 
		System.out.println("fetching data...");
	 
		// Get the current orderbook
		OrderBook orderBook = marketDataService.getPartialOrderBook(Currencies.BTC, Currencies.USD);
	 
		System.out.println("received data.");
	 
		System.out.println("plotting...");
	 
		// Create Chart
		Chart chart = new Chart(800, 500);
	 
		// Customize Chart
		//		chart.setChartTitle("MtGox Order Book");
		chart.setYAxisTitle("BTC");
		chart.setXAxisTitle("USD");
		//		chart.getStyleManager().setChartType(ChartType.Area);
	 
		// BIDS
		List<Number> xData = new ArrayList<Number>();
		List<Number> yData = new ArrayList<Number>();
		BigDecimal accumulatedBidUnits = new BigDecimal("0");
		for (LimitOrder limitOrder : orderBook.getBids()) {
			xData.add(limitOrder.getLimitPrice().getAmount());
			accumulatedBidUnits = accumulatedBidUnits.add(limitOrder.getTradableAmount());
			yData.add(accumulatedBidUnits);
		}
		Collections.reverse(xData);
		Collections.reverse(yData);
	 
		// Bids Series
		Series series = chart.addSeries("bids", xData, yData);
		series.setMarker(SeriesMarker.NONE);
	 
		// ASKS
		xData = new ArrayList<Number>();
		yData = new ArrayList<Number>();
		BigDecimal accumulatedAskUnits = new BigDecimal("0");
		for (LimitOrder limitOrder : orderBook.getAsks()) {
			xData.add(limitOrder.getLimitPrice().getAmount());
			accumulatedAskUnits = accumulatedAskUnits.add(limitOrder.getTradableAmount());
			yData.add(accumulatedAskUnits);
		}
	 
		// Asks Series
		series = chart.addSeries("asks", xData, yData);
		series.setMarker(SeriesMarker.NONE);
	 
		new SwingWrapper(chart).displayChart();
	 
	}
}

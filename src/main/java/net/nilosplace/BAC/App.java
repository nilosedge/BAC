package net.nilosplace.BAC;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;

import net.nilosplace.BAC.TransferFunds.Direction;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeException;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.NotAvailableFromExchangeException;
import com.xeiam.xchange.NotYetImplementedForExchangeException;
import com.xeiam.xchange.bitstamp.BitstampExchange;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.mtgox.v2.MtGoxExchange;
import com.xeiam.xchange.service.polling.PollingMarketDataService;

public class App {
	public static void main(String[] args) throws ExchangeException, NotAvailableFromExchangeException, NotYetImplementedForExchangeException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		ExchangeSpecification mtgoxSpec = new ExchangeSpecification(MtGoxExchange.class.getName());
		Exchange mtgox = ExchangeFactory.INSTANCE.createExchange(mtgoxSpec);

		ExchangeSpecification bitStampSpec = new ExchangeSpecification(BitstampExchange.class.getName());
		Exchange bitstamp = ExchangeFactory.INSTANCE.createExchange(bitStampSpec);
		
		//ExchangeSpecification BTCESpec = new ExchangeSpecification(BTCEExchange.class.getName());
		//Exchange btce = ExchangeFactory.INSTANCE.createExchange(BTCESpec);
		
		PollingMarketDataService mtgoxDataService = mtgox.getPollingMarketDataService();
		PollingMarketDataService bitstampDataService = bitstamp.getPollingMarketDataService();

		long glast = 0;
		long gcurrent = 1;
		long blast = 1;
		long bcurrent = 0;
		
		TransferFunds funds = new TransferFunds(new BigDecimal(Double.parseDouble("434.94")).setScale(2, RoundingMode.DOWN), Direction.BITSTAMP);

		DataSaver save = new DataSaver();
		DataCollector collect = new DataCollector(funds);
		
		while(true) {
			try {
				Ticker mtgoxTicker = mtgoxDataService.getTicker("BTC", "USD");
				Ticker bitstampTicker = bitstampDataService.getTicker("BTC", "USD");

				gcurrent = mtgoxTicker.getTimestamp().getTime();
				bcurrent = bitstampTicker.getTimestamp().getTime();
				
				if(gcurrent != glast || bcurrent != blast) {
					save.saveData(mtgoxTicker, bitstampTicker);
					collect.collectData(mtgoxTicker, bitstampTicker);

					funds.attemptTransfer(mtgoxTicker, bitstampTicker, collect.getzValue());
					
					collect.updateCharts();
					
					glast = gcurrent;
					blast = bcurrent;
				}

				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	

}

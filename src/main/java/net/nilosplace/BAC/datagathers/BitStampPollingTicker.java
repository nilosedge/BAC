package net.nilosplace.BAC.datagathers;

import java.io.IOException;
import java.sql.Connection;

import net.nilosplace.BAC.util.ExchangeDataSaver;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeException;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.NotAvailableFromExchangeException;
import com.xeiam.xchange.NotYetImplementedForExchangeException;
import com.xeiam.xchange.bitstamp.BitstampExchange;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.service.polling.PollingMarketDataService;

public class BitStampPollingTicker extends ExchangeDataSaver {
	
	public BitStampPollingTicker(Connection c) {
		super(c);
	}
	
	public void run() {
		ExchangeSpecification bitStampSpec = new ExchangeSpecification(BitstampExchange.class.getName());
		bitStampSpec.setSslUri("https://bitstamp.net");
		Exchange bitstamp = ExchangeFactory.INSTANCE.createExchange(bitStampSpec);
		
		PollingMarketDataService bitstampDataService = bitstamp.getPollingMarketDataService();
		
		while(true) {
			try {
				Ticker bitstampTicker = bitstampDataService.getTicker("BTC", "USD");
				saveTickerData("bitstamp", bitstampTicker);
				sleep(1000);
			} catch (ExchangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotAvailableFromExchangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NotYetImplementedForExchangeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}

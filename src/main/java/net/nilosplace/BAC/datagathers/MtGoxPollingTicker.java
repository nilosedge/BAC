package net.nilosplace.BAC.datagathers;

import java.io.IOException;
import java.sql.Connection;

import net.nilosplace.BAC.util.ExchangeDataSaver;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeException;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.NotAvailableFromExchangeException;
import com.xeiam.xchange.NotYetImplementedForExchangeException;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.mtgox.v2.MtGoxExchange;
import com.xeiam.xchange.service.polling.PollingMarketDataService;

public class MtGoxPollingTicker extends ExchangeDataSaver {
	
	public MtGoxPollingTicker(Connection c) {
		super(c);
	}
	
	public void run() {
		ExchangeSpecification mtgoxSpec = new ExchangeSpecification(MtGoxExchange.class.getName());
		mtgoxSpec.setSslUri("https://mtgox.com");
		Exchange mtgox = ExchangeFactory.INSTANCE.createExchange(mtgoxSpec);
		PollingMarketDataService mtgoxDataService = mtgox.getPollingMarketDataService();
		try {
			while(true) {
				Ticker mtgoxTicker = mtgoxDataService.getTicker("BTC", "USD");
				saveTickerData("mtgox", mtgoxTicker);
				sleep(1000);
			}
		} catch (ExchangeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotAvailableFromExchangeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotYetImplementedForExchangeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

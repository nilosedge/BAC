package net.nilosplace.BAC.datagathers;

import java.sql.Connection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import net.nilosplace.BAC.util.StreamDataSaver;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.mtgox.v2.MtGoxExchange;
import com.xeiam.xchange.mtgox.v2.service.streaming.MtGoxStreamingConfiguration;
import com.xeiam.xchange.service.streaming.ExchangeStreamingConfiguration;
import com.xeiam.xchange.service.streaming.StreamingExchangeService;

public class MtGoxStreaming extends Thread {

	private Connection connection;
	private String channel;
	
	public MtGoxStreaming(Connection connection, String channel) {
		this.connection = connection;
		this.channel = channel;
	}
	
	public void run() {
		Exchange mtGoxExchange = ExchangeFactory.INSTANCE.createExchange(MtGoxExchange.class.getName());
		 
		ExchangeStreamingConfiguration mtGoxStreamingConfiguration = new MtGoxStreamingConfiguration(10, 10000, 60000, true, channel);
			
		StreamingExchangeService streamingMarketDataService = mtGoxExchange.getStreamingExchangeService(mtGoxStreamingConfiguration);
			
		streamingMarketDataService.connect();
 
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		Future<?> mtGoxMarketDataFuture = executorService.submit(new StreamDataSaver(streamingMarketDataService, connection, "mtgox"));
	 
		try {
			mtGoxMarketDataFuture.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
		executorService.shutdown();
	 
		System.out.println(Thread.currentThread().getName() + ": Disconnecting...");
		streamingMarketDataService.disconnect();
	}
}

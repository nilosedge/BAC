package net.nilosplace.BAC;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import net.nilosplace.BAC.charts.DiffChart;
import net.nilosplace.BAC.charts.SDChart;
import net.nilosplace.BAC.charts.MoneyChart;
import net.nilosplace.BAC.charts.ValueChart;

import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchart.SwingWrapper;

public class DataCollector {
	
	private List<BigDecimal> difflist = new ArrayList<BigDecimal>();
	private List<BigDecimal> sds = new ArrayList<BigDecimal>();
	private List<BigDecimal> means = new ArrayList<BigDecimal>();
	private List<BigDecimal> top_per = new ArrayList<BigDecimal>();
	private List<BigDecimal> bottom_per = new ArrayList<BigDecimal>();
	private List<BigDecimal> money = new ArrayList<BigDecimal>();
	private List<BigDecimal> zs = new ArrayList<BigDecimal>();
	private List<BigDecimal> mtgox = new ArrayList<BigDecimal>();
	private List<BigDecimal> bitstamp = new ArrayList<BigDecimal>();
	
	
	private TransferFunds funds;
	private MoneyChart moneyChart;
	private DiffChart diffChart;
	private SDChart sdChart;
	private ValueChart valueChart;
	private int windowSide = 60;
	private BigDecimal zValue = BigDecimal.ZERO;

	public DataCollector(TransferFunds funds) {
		this.funds = funds;
		initDiffChart();
		initMoneyChart();
		initSDChart();
		initValueChart();
	}
	
	private void initMoneyChart() {
		moneyChart = new MoneyChart(money);
		new SwingWrapper(moneyChart.getChart()).displayChart();
	}
	
	private void initDiffChart() {
		diffChart = new DiffChart(difflist, means);
		new SwingWrapper(diffChart.getChart()).displayChart();
	}
	
	private void initSDChart() {
		sdChart = new SDChart(sds, top_per, bottom_per);
		new SwingWrapper(sdChart.getChart()).displayChart();
	}
	
	private void initValueChart() {
		// This needs to change also
		valueChart = new ValueChart(mtgox, bitstamp);
		new SwingWrapper(valueChart.getChart()).displayChart();
	}

	public void collectData(Ticker mtgoxTicker, Ticker bitstampTicker) {
		mtgox.add(mtgoxTicker.getLast().getAmount());
		bitstamp.add(bitstampTicker.getLast().getAmount());
		BigDecimal diff = calcDiff(mtgoxTicker, bitstampTicker);
		difflist.add(diff);
		BigDecimal top = diff.divide(mtgoxTicker.getLast().getAmount(), RoundingMode.FLOOR).multiply(BigDecimal.valueOf(100));
		top_per.add(top);
		BigDecimal bottom = diff.divide(bitstampTicker.getLast().getAmount(), RoundingMode.CEILING).multiply(BigDecimal.valueOf(100));
		bottom_per.add(bottom);
		
		List<BigDecimal> subList = difflist.subList((difflist.size() - windowSide) > 0 ? (difflist.size() - windowSide): 0, difflist.size());
		
		
		BigDecimal mean = calcMean(subList);
		means.add(mean);
		BigDecimal sd = calcSD(subList);
		sds.add(sd);
		zValue = calcZ(diff, mean, sd);
		zs.add(zValue);
		

		money.add(funds.getAmount());
		
		System.out.println(
			top + "% " +
			mtgoxTicker.getLast().getAmount() + " " +
			diff + " " +
			bitstampTicker.getLast().getAmount() + " " +
			bottom + "% SD: " + sd.setScale(4, RoundingMode.DOWN) + " Mean: " + mean.setScale(4, RoundingMode.DOWN) + " Z: " + zValue
		);

	}

	public void updateCharts() {
		moneyChart.updateChart();
		diffChart.updateChart();
		sdChart.updateChart();
		valueChart.updateChart();
	}
	
	public BigDecimal calcDiff(Ticker top, Ticker bottom) {
		return top.getLast().getAmount().subtract(bottom.getLast().getAmount());
	}
	
	public BigDecimal calcMean(List<BigDecimal> in) {
		BigDecimal mean = BigDecimal.ZERO;
		for(int i = 0; i < in.size(); i++) {
			mean = mean.add(in.get(i));
		}
		return mean.divide(BigDecimal.valueOf(in.size()), RoundingMode.HALF_EVEN);
	}
	
	public BigDecimal calcSD(List<BigDecimal> in) {
		BigDecimal mean = calcMean(in);
		BigDecimal var = BigDecimal.ZERO;
		for(int i = 0; i < in.size(); i++) {
			var = var.add(in.get(i).subtract(mean).pow(2));
		}
		if(in.size() > 0) {
			var = var.divide(BigDecimal.valueOf(in.size()), RoundingMode.HALF_EVEN);
			return BigDecimal.valueOf(Math.sqrt(var.doubleValue()));
		} else {
			return BigDecimal.ZERO;
		}
	}
	
	public BigDecimal calcZ(BigDecimal current, BigDecimal mean, BigDecimal sd) {
		if(sd.doubleValue() > 0) {
			return current.subtract(mean).divide(sd, RoundingMode.HALF_EVEN);
		} else {
			return BigDecimal.ZERO;
		}
	}

	public BigDecimal getzValue() {
		return zValue;
	}

	public void setzValue(BigDecimal zValue) {
		this.zValue = zValue;
	}
}

package net.nilosplace.BAC;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.xeiam.xchange.dto.marketdata.Ticker;

public class TransferFunds {
	
	private BigDecimal amount = BigDecimal.ZERO;
	private Direction dir;
	private BigDecimal mtgox_fee = new BigDecimal(Double.parseDouble(".006"));
	private BigDecimal bitstamp_fee = new BigDecimal(Double.parseDouble(".005"));
	
	private BigDecimal lastBitstamp = BigDecimal.ZERO;
	private BigDecimal lastMtgox = BigDecimal.ZERO;

	public TransferFunds(BigDecimal startAmount, Direction dir) {
		amount = startAmount;
		if(dir.equals(Direction.MTGOX)) {
			lastMtgox = amount;
		} else {
			lastBitstamp = amount;
		}
		this.dir = dir;
	}
	
	public BigDecimal attemptTransfer(Ticker mtgoxTicker, Ticker bitstampTicker, BigDecimal zvalue) {
		//BigDecimal diff = mtgox.subtract(bitstamp);
		
		if(dir.equals(Direction.MTGOX)) {
			//BigDecimal mtgox_per = diff.divide(mtgox, RoundingMode.FLOOR).multiply(BigDecimal.valueOf(100));
			if(zvalue.doubleValue() < -1.5) {
				BigDecimal bitcoins = amount.divide(mtgoxTicker.getLast().getAmount(), RoundingMode.FLOOR);
				BigDecimal mtgox_fees = bitcoins.multiply(mtgox_fee);
				bitcoins = bitcoins.subtract(mtgox_fees);
				BigDecimal newAmount = bitcoins.multiply(bitstampTicker.getLast().getAmount());
				BigDecimal bitstamp_fees = newAmount.multiply(bitstamp_fee); 
				newAmount = newAmount.subtract(bitstamp_fees).setScale(2, RoundingMode.DOWN);
				
				System.out.println("New Amount would be: " + newAmount + " Last Amount: " + lastBitstamp);
				if(newAmount.compareTo(lastBitstamp) > 0) {
					System.out.println("BitStamp Fee: " + bitstamp_fees.setScale(2, RoundingMode.CEILING));
					System.out.println("MtGox Fee: " + amount.multiply(mtgox_fees).setScale(2, RoundingMode.CEILING));
					System.out.println("Transfering from MtGox to BitStamp: " + amount + " new Amount: " + newAmount);
					lastBitstamp = amount;
					amount = newAmount;
					dir = Direction.BITSTAMP;
				}
				
			}
		} else if(dir.equals(Direction.BITSTAMP)){
			//BigDecimal bitstamp_per = diff.divide(bitstamp, RoundingMode.CEILING).multiply(BigDecimal.valueOf(100));
			if(zvalue.doubleValue() > 1.5) {
				BigDecimal bitcoins = amount.divide(bitstampTicker.getLast().getAmount(), RoundingMode.FLOOR);
				BigDecimal bitstamp_fees = bitcoins.multiply(bitstamp_fee);
				bitcoins = bitcoins.subtract(bitstamp_fees);
				BigDecimal newAmount = bitcoins.multiply(mtgoxTicker.getLast().getAmount());
				BigDecimal mtgox_fees = newAmount.multiply(mtgox_fee); 
				newAmount = newAmount.subtract(mtgox_fees).setScale(2, RoundingMode.DOWN);

				System.out.println("New Amount would be: " + newAmount + " Last Amount: " + lastMtgox);
				if(newAmount.compareTo(lastMtgox) > 0) {
					System.out.println("MtGox Fee: " + mtgox_fees.setScale(2, RoundingMode.CEILING));
					System.out.println("BitStamp Fee: " + amount.multiply(bitstamp_fees).setScale(2, RoundingMode.CEILING));
					System.out.println("Transfering from BitStamp to MtGox: " + amount + " new Amount: " + newAmount);
					lastMtgox = amount;
					amount = newAmount;
					dir = Direction.MTGOX;
				}
			}
		}
		return amount;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public enum Direction {
		MTGOX,
		BITSTAMP
	}
}

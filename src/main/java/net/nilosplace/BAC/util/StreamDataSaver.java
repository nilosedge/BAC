package net.nilosplace.BAC.util;

import java.sql.Connection;

import com.xeiam.xchange.dto.marketdata.OrderBookUpdate;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.dto.marketdata.Trade;
import com.xeiam.xchange.service.streaming.ExchangeEvent;
import com.xeiam.xchange.service.streaming.StreamingExchangeService;

public class StreamDataSaver extends ExchangeDataSaver implements Runnable {

	private StreamingExchangeService streamingExchangeService;
	private String exchange;
	 
	public StreamDataSaver(StreamingExchangeService streamingExchangeService, Connection c, String exchange) {
		super(c);
		this.exchange = exchange;
		this.streamingExchangeService = streamingExchangeService;
	}
	
	public void run() {
		try {
			while (true) {
				ExchangeEvent exchangeEvent = streamingExchangeService.getNextEvent();
				switch (exchangeEvent.getEventType()) {
					case CONNECT:
						System.out.println("Connected to " + exchange + "!");
						break;
					case TICKER:
						Ticker ticker = (Ticker) exchangeEvent.getPayload();
						saveTickerData(exchange, ticker);
						break;
					case TRADE:
						Trade trade = (Trade) exchangeEvent.getPayload();
						saveTradeData(exchange, trade);
						break;
					case DEPTH:
						OrderBookUpdate orderBookUpdate = (OrderBookUpdate) exchangeEvent.getPayload();
						saveDepthData(exchange, orderBookUpdate);
						break;
					case DISCONNECT:
						System.out.println("Disconnected from " + exchange + "!");
						break;
					default:
						System.out.println(exchangeEvent.getEventType());
						break;
				}
			}
		} catch (InterruptedException e) {
			System.out.println("ERROR in Runnable!!!");
		}
	}
}

package net.nilosplace.BAC.util;

import java.sql.Connection;
import java.sql.SQLException;

import com.xeiam.xchange.dto.marketdata.OrderBookUpdate;
import com.xeiam.xchange.dto.marketdata.Ticker;
import com.xeiam.xchange.dto.marketdata.Trade;

public class ExchangeDataSaver extends Thread {

	protected Connection sharedConnection;

	public ExchangeDataSaver(Connection c) {
		// Tables
		// CREATE TABLE datatable (id INTEGER PRIMARY KEY, mtgox VARCHAR(255), bitstamp VARCHAR(255), current_time VARCHAR(255));
		// CREATE TABLE tickers   (id INTEGER PRIMARY KEY, exchange VARCHAR(255), currency VARCHAR(255), last VARCHAR(255), bid VARCHAR(255), ask VARCHAR(255), high VARCHAR(255), low VARCHAR(255), volume VARCHAR(255), current_time VARCHAR(255));
		// CREATE TABLE trades    (id INTEGER PRIMARY KEY, exchange VARCHAR(255), currency VARCHAR(255), type VARCHAR(255), amount VARCHAR(255), price VARCHAR(255), current_time VARCHAR(255))
		sharedConnection = c;
	}
	
	public void saveTickerData(String exchange, Ticker ticker) {

		String insert = "insert into tickers (exchange, currency, last, bid, ask, high, low, volume, current_time) values(\"" +
			exchange + "\", \"" +
			ticker.getLow().getCurrencyUnit() + "\", \"" +
			ticker.getLast().getAmount().doubleValue() + "\", \"" +
			ticker.getBid().getAmount().doubleValue() + "\", \"" +
			ticker.getAsk().getAmount().doubleValue() + "\", \"" +
			ticker.getHigh().getAmount().doubleValue() + "\", \"" +
			ticker.getLow().getAmount().doubleValue() + "\", \"" +
			ticker.getVolume().doubleValue() + "\", \"" +
			ticker.getTimestamp().getTime() + "\")";

		try {
			sharedConnection.createStatement().execute(insert);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void saveDepthData(String exchange, OrderBookUpdate orderBookUpdate) {
		System.out.println(orderBookUpdate);
	}
	
	protected void saveTradeData(String exchange, Trade trade) {

		String insert = "insert into trades (exchange, currency, type, amount, price, current_time) values(\"" +
			exchange + "\", \"" +
			trade.getTransactionCurrency() + "\", \"" +
			trade.getType() + "\", \"" +
			trade.getTradableAmount().doubleValue() + "\", \"" +
			trade.getPrice().getAmount().doubleValue() + "\", \"" +
			trade.getTimestamp().getTime() + "\")";
		try {
			sharedConnection.createStatement().execute(insert);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

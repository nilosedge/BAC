package net.nilosplace.BAC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import net.nilosplace.BAC.datagathers.BitStampPollingTicker;
import net.nilosplace.BAC.datagathers.MtGoxStreaming;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		Connection c = DriverManager.getConnection("jdbc:sqlite:data.sqlite");
		
		//MtGoxPollingTicker mtgoxTicker = new MtGoxPollingTicker(c);
		//mtgoxTicker.start();

		BitStampPollingTicker bitStampTicker = new BitStampPollingTicker(c);
		bitStampTicker.start();
		
		MtGoxStreaming mgs1 = new MtGoxStreaming(c, "trade.BTC");
		mgs1.start();
		MtGoxStreaming mgs2 = new MtGoxStreaming(c, "ticker.BTCUSD");
		mgs2.start();
		//MtGoxStreaming mgs3 = new MtGoxStreaming(c, "depth.BTCUSD");
		//mgs3.start();
		
	}
}
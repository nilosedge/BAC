package net.nilosplace.BAC.charts;

import java.math.BigDecimal;
import java.util.List;

import com.xeiam.xchart.SeriesColor;

public class DiffChart extends DefaultChart {

	private List<BigDecimal> difflist;
	private List<BigDecimal> means;

	public DiffChart(List<BigDecimal> difflist, List<BigDecimal> means) {
		super("Diff");
		this.difflist = difflist;
		this.means = means;
	}
	
	public void updateChart() {
		chart.getSeriesMap().clear();
		addSeries("Diff", difflist, SeriesColor.BLUE, SeriesColor.BLUE);
		addSeries("Mean", means, SeriesColor.BLACK, SeriesColor.BLACK);
	}

}

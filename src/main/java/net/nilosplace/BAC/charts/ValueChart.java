package net.nilosplace.BAC.charts;

import java.math.BigDecimal;
import java.util.List;

import com.xeiam.xchart.SeriesColor;

public class ValueChart extends DefaultChart {

	private List<BigDecimal> mtgox;
	private List<BigDecimal> bitstamp;

	public ValueChart(List<BigDecimal> mtgox, List<BigDecimal> bitstamp) {
		super("Value");
		this.mtgox = mtgox;
		this.bitstamp = bitstamp;
	}
	
	public void updateChart() {
		chart.getSeriesMap().clear();
		addSeries("MtGox", mtgox, SeriesColor.BLUE, SeriesColor.BLUE);
		addSeries("Bitstamp", bitstamp, SeriesColor.RED, SeriesColor.RED);
	}

}

package net.nilosplace.BAC.charts;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.xeiam.xchart.Chart;
import com.xeiam.xchart.Series;
import com.xeiam.xchart.SeriesColor;
import com.xeiam.xchart.SeriesLineStyle;
import com.xeiam.xchart.SeriesMarker;
import com.xeiam.xchart.demo.charts.ExampleChart;

public class DefaultChart implements ExampleChart {

	private String name;
	protected Chart chart;
	protected Series series;
	
	public DefaultChart(String name) {
		this.name = name;
	}
	
	protected void addSeries(String name, List<BigDecimal> list, SeriesColor lineColor, SeriesColor dotColor) {
		ArrayList<Number> x = new ArrayList<Number>();
		ArrayList<Number> y = new ArrayList<Number>();
		
		for(int i = 0; i < list.size(); i++) {
			x.add(i);
			y.add(list.get(i).doubleValue());
		}
		
		series = chart.addSeries(name, x, y);
		series.setLineColor(lineColor);
		series.setLineStyle(SeriesLineStyle.SOLID);
		series.setMarkerColor(dotColor);
		series.setMarker(SeriesMarker.CIRCLE);
	}
	
	public Chart getChart() {
		chart = new Chart(400, 300);
		chart.setChartTitle(name);
		chart.setXAxisTitle("X");
		chart.setYAxisTitle("Y");
		chart.getStyleManager().setLegendVisible(false);
		return chart;
	}

	
	
}

package net.nilosplace.BAC.charts;

import java.math.BigDecimal;
import java.util.List;

import com.xeiam.xchart.SeriesColor;

public class MoneyChart extends DefaultChart {

	private List<BigDecimal> list;

	public MoneyChart(List<BigDecimal> list) {
		super("Money");
		this.list = list;
	}
	
	public void updateChart() {
		chart.getSeriesMap().clear();
		addSeries("Diff", list, SeriesColor.BLUE, SeriesColor.BLUE);
	}

}

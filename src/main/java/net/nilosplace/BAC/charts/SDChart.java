package net.nilosplace.BAC.charts;

import java.math.BigDecimal;
import java.util.List;

import com.xeiam.xchart.SeriesColor;

public class SDChart extends DefaultChart {

	private List<BigDecimal> sds;
	private List<BigDecimal> top_per;
	private List<BigDecimal> bottom_per;
	
	public SDChart(List<BigDecimal> sds, List<BigDecimal> top_per, List<BigDecimal> bottom_per) {
		super("SD");
		this.sds = sds;
		this.top_per = top_per;
		this.bottom_per = bottom_per;
	}
	
	public void updateChart() {
		chart.getSeriesMap().clear();
		addSeries("SD", sds, SeriesColor.RED, SeriesColor.RED);
		addSeries("Top%", top_per, SeriesColor.GREEN, SeriesColor.GREEN);
		addSeries("Bot%", bottom_per, SeriesColor.CYAN, SeriesColor.CYAN);
	}

}
